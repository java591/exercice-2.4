package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Date;
import java.util.Objects;

/**
 * POJO model représentant le Badge Digital
 */
public class DigitalBadge implements Comparable<DigitalBadge>{

    private DigitalBadgeMetadata metadata;
    private File badge;
    private Date begin;
    private Date end;
    private String serial;

    /**
     * Constructeur
     *
     * @param metadata
     * @param badge
     */
    public DigitalBadge(String serial, Date begin,Date end, DigitalBadgeMetadata metadata, File badge) {
        this.metadata = metadata;
        this.badge = badge;
    }

    /**
     * retourne la date de debut de validité du badge
     * @return
     */
    public Date getBegin(){return this.begin;}
    /**
     * définit la date de debut de validité du badge
     * @return
     */
    public void setBegin(Date begin){this.begin=begin;}
    /**
     * retourne la date de fin de validité du badge
     * @return
     */
    public Date getEnd(){return this.end;}
    /**
     * definit la date de fin de validité du badge
     * @return
     */
    public void setEnd(Date end){this.end=end;}
    /**
     * retourne le serial du badge
     * @return
     */
    public String getSerial(){return this.serial;}
    /**
     * définit le serial du badge
     * @return
     */
    public void setSerial(String serial) {this.serial = serial;}

    /**
     * Getter des métadonnées du badge
     * @return les métadonnées DigitalBadgeMetadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Setter des métadonnées du badge
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge (l'image)
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Setter du badge (Fichier image)
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    /**
     * {@inheritDoc}
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return begin.equals(that.begin) && end.equals(that.end)&& serial.equals(that.serial)&&metadata.equals(that.metadata) && badge.equals(that.badge);
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(metadata, badge);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", badge=" + badge +
                '}';
    }

    @Override
    public int compareTo(DigitalBadge o) {
        return this.metadata.compareTo(o.metadata);
    }
}
